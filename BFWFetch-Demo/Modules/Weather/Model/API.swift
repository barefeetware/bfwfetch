//
//  API.swift
//  BFWFetch-Demo
//
//  Created by Tom Brodhurst-Hill on 31/3/21.
//  Copyright © 2021 BareFeetWare. All rights reserved.
//

import Foundation

/// Domain name scope for all types specific to API requests and responses.
enum API {}
